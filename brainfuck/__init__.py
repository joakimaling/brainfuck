#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# brainfuck/__init__.py
#

from typing import Dict, List
from sys import stdin, stdout
from termios import TCSADRAIN, tcgetattr, tcsetattr
from tty import setraw


def _fetch() -> str:
	"""
	"""
	descriptor: int = stdin.fileno()
	settings: list = tcgetattr(descriptor)

	try:
		setraw(stdin.fileno())
		glyph: str = stdin.read(1)

	finally:
		tcsetattr(descriptor, TCSADRAIN, settings)

	return glyph


class Brainfuck:
	"""
	Simple Brainfuck Parser.
	"""

	@staticmethod
	def _build(script: str) -> Dict[int, int]:
		"""
		"""
		brackets: Dict[int, int] = {}
		stack: List[int] = []

		for position, command in enumerate(script):
			if command == '[':
				stack.append(position)

			if command == ']':
				start = stack.pop()
				brackets[start] = position
				brackets[position] = start

		return brackets

	@staticmethod
	def _clean(script: str) -> str:
		"""
		Removes any character not part of the Brainfuck language such as
		comments.
		"""
		return ''.join(glyph for glyph in script if glyph in '><+-[].,')

	def parse(self, script: str) -> int:
		"""
		Walks through the script string acting upon each glyph it encounters.
		"""
		cells: List[int] = [0]
		cell_index: int = 0
		code_index: int = 0

		script: str = self._clean(script)
		brackets: Dict[int, int] = self._build(script)

		while code_index < len(script):
			command: str = script[code_index]

			if command == '>':
				cell_index += 1
				if cell_index == len(cells):
					cells.append(0)

			if command == '<':
				cell_index = 0 if cell_index <= 0 else cell_index - 1

			if command == '+':
				cells[cell_index] = cells[cell_index] + 1 if cells[cell_index] < 255 else 0

			if command == '-':
				cells[cell_index] = cells[cell_index] - 1 if cells[cell_index] > 0 else 255

			if command == '[' and cells[cell_index] == 0:
				code_index = brackets[code_index]

			if command == ']' and cells[cell_index] != 0:
				code_index = brackets[code_index]

			if command == '.':
				stdout.write(chr(cells[cell_index]))

			if command == ',':
				cells[cell_index] = ord(_fetch())

			code_index += 1

		return 0

	def parse_file(self, script: str) -> int:
		"""
		"""
		try:
			with open(script, 'r') as file:
				return self.parse(file.read())
		except FileNotFoundError:
			return 1
