#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# main.py
#

# Custom libraries
from brainfuck import Brainfuck

# Generic libraries
from argparse import ArgumentParser, Namespace
from sys import argv, exit
from typing import List


def main(arguments: List[str] | None) -> int:
	"""
	Handles the parsing of arguments & passing them to the Brainfuck parser.
	"""
	parser: ArgumentParser = ArgumentParser(
		description='Simple Brainfuck Parser',
		epilog='(c) 2022 Joakim Åling'
	)

	# Populate the argument parser
	# Brainfuck script or name of script file
	parser.add_argument(
		'script',
		help='the brainfuck script to parse'
	)

	# Flag to indicate the script is to be read from a file
	parser.add_argument(
		'-f',
		'--file',
		action='store_true',
		help='treats the value of script as the path to a file'
	)

	# Run the parser and return given arguments
	data: Namespace = parser.parse_args(arguments)

	# Instantiates the class
	brainfuck = Brainfuck()

	if data.file:
		# Load script form file
		return brainfuck.parse_file(data.script)

	# Parse as is
	return brainfuck.parse(data.script)


if __name__ == '__main__':
	exit(main(argv[1:]))
